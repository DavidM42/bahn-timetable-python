# Event

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ppth** | **str** | Planned Path. A sequence of station names separated by the pipe symbols (&#39;|&#39;). E.g.: &#39;Mainz Hbf|Rüsselsheim|Frankfrt(M) Flughafen&#39;. For arrival, the path indicates the stations that come before the current station. The first element then is the trip&#39;s start station. For departure, the path indicates the stations that come after the current station. The last element in the path then is the trip&#39;s destination station. Note that the current station is never included in the path (neither for arrival nor for departure).  | [optional] 
**cpth** | **str** | Changed path. | [optional] 
**pp** | **str** | Planned platform. | [optional] 
**cp** | **str** | Changed platform. | [optional] 
**pt** | **str** | Planned time. Planned departure or arrival time. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**ct** | **str** | Changed time. New estimated or actual departure or arrival time. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**ps** | [**EventStatus**](EventStatus.md) |  | [optional] 
**cs** | [**EventStatus**](EventStatus.md) | Changed status. The status of this event, a one-character indicator that is one of: * &#39;a&#39; &#x3D; this event was added * &#39;c&#39; &#x3D; this event was cancelled * &#39;p&#39; &#x3D; this event was planned (also used when the cancellation of an event has been revoked) The status applies to the event, not to the trip as a whole. Insertion or removal of a single stop will usually affect two events at once: one arrival and one departure event. Note that these two events do not have to belong to the same stop. For example, removing the last stop of a trip will result in arrival cancellation for the last stop and of departure cancellation for the stop before the last. So asymmetric cancellations of just arrival or departure for a stop can occur.  | [optional] 
**hi** | **int** | Hidden. 1 if the event should not be shown on WBT because travellers are not supposed to enter or exit the train at this stop. | [optional] 
**clt** | **str** | Cancellation time. Time when the cancellation of this stop was created. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**wings** | **str** | Wing. A sequence of trip id separated by the pipe symbols (&#39;|&#39;). E.g. &#39;-906407760000782942-1403311431&#39;. | [optional] 
**tra** | **str** | Transition. Trip id of the next or previous train of a shared train. At the start stop this references the previous trip, at the last stop it references the next trip. E.g. &#39;2016448009055686515-1403311438-1&#39; | [optional] 
**pde** | **str** | Planned distant endpoint. | [optional] 
**cde** | **str** | Changed distant endpoint. | [optional] 
**dc** | **int** | Distant change. | [optional] 
**l** | **str** | Line. The line indicator (e.g. \&quot;3\&quot; for an S-Bahn or \&quot;45S\&quot; for a bus). | [optional] 
**m** | [**list[Message]**](Message.md) | List of messages. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


