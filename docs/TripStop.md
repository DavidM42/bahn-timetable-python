# TripStop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**i** | **int** | Stop index. | 
**eva** | **int** | EVA station number. | 
**jt** | [**JunctionType**](JunctionType.md) |  | [optional] 
**ar** | [**Event**](Event.md) |  | [optional] 
**dp** | [**Event**](Event.md) |  | [optional] 
**m** | [**list[Message]**](Message.md) | Message element. | [optional] 
**hd** | [**list[HistoricDelay]**](HistoricDelay.md) | Historic delay element. | [optional] 
**hpc** | [**list[HistoricPlatformChange]**](HistoricPlatformChange.md) | Historic platform change element. | [optional] 
**conn** | [**list[Connection]**](Connection.md) | Connection element. | [optional] 
**rtr** | [**list[ReferenceTripRelation]**](ReferenceTripRelation.md) | Reference trip relation element. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


