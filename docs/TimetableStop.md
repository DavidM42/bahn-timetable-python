# TimetableStop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | An id that uniquely identifies the stop. It consists of the following three elements separated by dashes * a &#39;daily trip id&#39; that uniquely identifies a trip within one day. This id is typically reused on subsequent days. This could be negative. * a 6-digit date specifier (YYMMdd) that indicates the planned departure date of the trip from its start station. * an index that indicates the position of the stop within the trip (in rare cases, one trip may arrive multiple times at one station). Added trips get indices above 100.  Example &#39;-7874571842864554321-1403311221-11&#39; would be used for a trip with daily trip id &#39;-7874571842864554321&#39; that starts on march the 31th 2014 and where the current station is the 11th stop.  | 
**eva** | **int** | The eva code of the station of this stop. Example &#39;8000105&#39; for Frankfurt(Main)Hbf. | 
**tl** | [**TripLabel**](TripLabel.md) | Trip label. | [optional] 
**ref** | [**TripReference**](TripReference.md) | Reference to an referenced trip. The substitution or additional trip references the originally planned trip. Note, referenced trip !&#x3D; reference trip | [optional] 
**ar** | [**Event**](Event.md) | Arrival element. This element does not have child elements. All information about the arrival is stored in attributes (see the next table). | [optional] 
**dp** | [**Event**](Event.md) | Departure element. This element does not have child elements. All information about the departure is stored in attributes (see the next table). | [optional] 
**m** | [**list[Message]**](Message.md) | Message element. | [optional] 
**hd** | [**list[HistoricDelay]**](HistoricDelay.md) | Historic delay element. | [optional] 
**hpc** | [**list[HistoricPlatformChange]**](HistoricPlatformChange.md) | Historic platform change element. | [optional] 
**conn** | [**list[Connection]**](Connection.md) | Connection element. | [optional] 
**rtr** | [**list[ReferenceTripRelation]**](ReferenceTripRelation.md) | Reference trip relation element. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


