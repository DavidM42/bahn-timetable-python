# MultipleTrips

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**trip** | [**list[Trip]**](Trip.md) | List of trip elements. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


