# swagger_client.DefaultApi

All URIs are relative to *https://api.deutschebahn.com/timetables/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fchg_eva_no_get**](DefaultApi.md#fchg_eva_no_get) | **GET** /fchg/{evaNo} | Returns all known changes for a station
[**plan_eva_no_date_hour_get**](DefaultApi.md#plan_eva_no_date_hour_get) | **GET** /plan/{evaNo}/{date}/{hour} | Returns planned data for the specified station within an hourly time slice 
[**rchg_eva_no_get**](DefaultApi.md#rchg_eva_no_get) | **GET** /rchg/{evaNo} | Returns all recent changes for a station 
[**station_pattern_get**](DefaultApi.md#station_pattern_get) | **GET** /station/{pattern} | Returns information about stations matching the given pattern


# **fchg_eva_no_get**
> Timetable fchg_eva_no_get(eva_no)

Returns all known changes for a station

Returns a Timetable object (see Timetable) that contains all known changes for the station given by evaNo.  The data includes all known changes from now on until ndefinitely into the future. Once changes become obsolete (because their trip departs from the station) they are removed from this resource.  Changes may include messages. On event level, they usually contain one or more of the 'changed' attributes ct, cp, cs or cpth. Changes may also include 'planned' attributes if there is no associated planned data for the change (e.g. an unplanned stop or trip).  Full changes are updated every 30s and should be cached for that period by web caches. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: Bearer
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
eva_no = 'eva_no_example' # str | Station EVA-number.

try:
    # Returns all known changes for a station
    api_response = api_instance.fchg_eva_no_get(eva_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->fchg_eva_no_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eva_no** | **str**| Station EVA-number. | 

### Return type

[**Timetable**](Timetable.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/xhtml+xml, application/xml, text/html
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plan_eva_no_date_hour_get**
> Timetable plan_eva_no_date_hour_get(eva_no, date, hour)

Returns planned data for the specified station within an hourly time slice 

Returns a Timetable object (see Timetable) that contains planned data for the specified station (evaNo) within the hourly time slice given by date (format YYMMDD) and hour (format HH). The data includes stops for all trips that arrive or depart within that slice. There is a small overlap between slices since some trips arrive in one slice and depart in another.  Planned data does never contain messages. On event level, planned data contains the 'plannned' attributes pt, pp, ps and ppth while the 'changed' attributes ct, cp, cs and cpth are absent.  Planned data is generated many hours in advance and is static, i.e. it does never change. It should be cached by web caches.public interface allows access to information about a station. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: Bearer
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
eva_no = 'eva_no_example' # str | Station EVA-number.
date = 'date_example' # str | Date in format YYMMDD.
hour = 'hour_example' # str | Hour in format HH.

try:
    # Returns planned data for the specified station within an hourly time slice 
    api_response = api_instance.plan_eva_no_date_hour_get(eva_no, date, hour)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->plan_eva_no_date_hour_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eva_no** | **str**| Station EVA-number. | 
 **date** | **str**| Date in format YYMMDD. | 
 **hour** | **str**| Hour in format HH. | 

### Return type

[**Timetable**](Timetable.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/xhtml+xml, application/xml, text/html
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **rchg_eva_no_get**
> Timetable rchg_eva_no_get(eva_no)

Returns all recent changes for a station 

Returns a Timetable object (see Timetable) that contains all recent changes for the station given by evaNo. Recent changes are always a subset of the full changes. They may equal full changes but are typically much smaller. Data includes only those changes that became known within the last 2 minutes.  A client that updates its state in intervals of less than 2 minutes should load full changes initially and then proceed to periodically load only the recent changes in order to save bandwidth.  Recent changes are updated every 30s as well and should be cached for that period by web caches. 

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: Bearer
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
eva_no = 'eva_no_example' # str | Station EVA-number.

try:
    # Returns all recent changes for a station 
    api_response = api_instance.rchg_eva_no_get(eva_no)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->rchg_eva_no_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **eva_no** | **str**| Station EVA-number. | 

### Return type

[**Timetable**](Timetable.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/xhtml+xml, application/xml, text/html
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **station_pattern_get**
> Stations station_pattern_get(pattern)

Returns information about stations matching the given pattern

This public interface allows access to information about a station.  See the document \"Types and XML Schema Definition\" in the documentation tab for information about the resulting data structure.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# Configure API key authorization: Bearer
configuration = swagger_client.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = swagger_client.DefaultApi(swagger_client.ApiClient(configuration))
pattern = 'pattern_example' # str | can be a station name (prefix), eva number, ds100/rl100 code, wildcard (*); doesn't seem to work with Umlauten in station name (prefix)

try:
    # Returns information about stations matching the given pattern
    api_response = api_instance.station_pattern_get(pattern)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->station_pattern_get: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pattern** | **str**| can be a station name (prefix), eva number, ds100/rl100 code, wildcard (*); doesn&#39;t seem to work with Umlauten in station name (prefix) | 

### Return type

[**Stations**](Stations.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/xhtml+xml, application/xml, text/html
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

