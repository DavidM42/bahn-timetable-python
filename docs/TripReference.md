# TripReference

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tl** | [**TripLabel**](TripLabel.md) |  | 
**rt** | [**list[TripLabel]**](TripLabel.md) | The referred trips reference trip elements. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


