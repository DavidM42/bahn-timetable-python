# StationData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**p** | **str** | List of platforms. A sequence of platforms separated by the pipe symbols (\&quot;|\&quot;). | [optional] 
**meta** | **str** | List of meta stations. A sequence of station names separated by the pipe symbols (\&quot;|\&quot;). | [optional] 
**name** | **str** | Station name. | [optional] 
**eva** | **int** | EVA station number. | [optional] 
**ds100** | **str** | DS100 station code. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


