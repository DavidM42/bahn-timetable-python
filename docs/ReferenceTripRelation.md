# ReferenceTripRelation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rt** | [**ReferenceTrip**](ReferenceTrip.md) |  | 
**rts** | [**ReferenceTripRelationToStop**](ReferenceTripRelationToStop.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


