# Connection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Id. | 
**ts** | **str** | Time stamp. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | 
**eva** | **int** | EVA station number. | [optional] 
**cs** | [**ConnectionStatus**](ConnectionStatus.md) | Connection status. | 
**ref** | [**TimetableStop**](TimetableStop.md) | Timetable stop of missed trip. | [optional] 
**s** | [**TimetableStop**](TimetableStop.md) | Timetable stop. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


