# Message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Message id. | 
**t** | [**MessageType**](MessageType.md) | Message type. | 
**_from** | **str** | Valid from. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**to** | **str** | Valid to. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**c** | **int** | Code. | [optional] 
**int** | **str** | Internal text. | [optional] 
**ext** | **str** | External text. | [optional] 
**cat** | **str** | Category. | [optional] 
**ec** | **str** | External category. | [optional] 
**ts** | **str** | Timestamp. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. \&quot;1404011437\&quot; for 14:37 on April the 1st of 2014. | 
**pr** | [**Priority**](Priority.md) |  | [optional] 
**o** | **str** | Owner. | [optional] 
**elnk** | **str** | External link associated with the message. | [optional] 
**_del** | **int** | Deleted. | [optional] 
**dm** | [**list[DistributorMessage]**](DistributorMessage.md) | Distributor message. | [optional] 
**tl** | [**list[TripLabel]**](TripLabel.md) | Trip label. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


