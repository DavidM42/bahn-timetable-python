# HistoricDelay

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ts** | **str** | Timestamp. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**ar** | **str** | The arrival event. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**dp** | **str** | The departure event. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 
**src** | [**DelaySource**](DelaySource.md) |  | [optional] 
**cod** | **str** | Detailed description of delay cause. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


