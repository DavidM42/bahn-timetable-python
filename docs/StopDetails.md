# StopDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Stop id. | 
**m** | [**list[Message]**](Message.md) | Message. | [optional] 
**conn** | [**list[Connection]**](Connection.md) | Message. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


