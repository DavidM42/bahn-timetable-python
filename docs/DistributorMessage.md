# DistributorMessage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**t** | [**DistributorType**](DistributorType.md) |  | [optional] 
**n** | **str** | Distributor name. | [optional] 
**int** | **str** | Internal text. | [optional] 
**ts** | **str** | Timestamp. The time, in ten digit &#39;YYMMddHHmm&#39; format, e.g. &#39;1404011437&#39; for 14:37 on April the 1st of 2014. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


