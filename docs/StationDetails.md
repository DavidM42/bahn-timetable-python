# StationDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**eva** | **int** | EVA station number. | 
**m** | [**list[Message]**](Message.md) | List of station based messages. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


