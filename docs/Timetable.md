# Timetable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**station** | **str** | Station name. | [optional] 
**eva** | **int** | EVA station number. | [optional] 
**s** | [**list[TimetableStop]**](TimetableStop.md) | List of TimetableStop. | [optional] 
**m** | [**list[Message]**](Message.md) | List of Message. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


