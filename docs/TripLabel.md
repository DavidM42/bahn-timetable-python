# TripLabel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**f** | **str** | Filter flags. | [optional] 
**t** | [**TripType**](TripType.md) | Trip type. | [optional] 
**o** | **str** | Owner. A unique short-form and only intended to map a trip to specific evu. | 
**n** | **str** | Trip/train number, e.g. \&quot;4523\&quot;. | 
**c** | **str** | Category. Trip category, e.g. \&quot;ICE\&quot; or \&quot;RE\&quot;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


