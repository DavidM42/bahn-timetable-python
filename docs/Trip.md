# Trip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Trip ID. | 
**tl** | [**TripLabel**](TripLabel.md) |  | 
**s** | [**list[TripStop]**](TripStop.md) | Trip stop. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


