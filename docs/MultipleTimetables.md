# MultipleTimetables

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timetable** | [**list[Timetable]**](Timetable.md) | List of timetables | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


