# ReferenceTripLabel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**n** | **str** | Trip/train number, e.g. \&quot;4523\&quot;. | 
**c** | **str** | Category. Trip category, e.g. \&quot;ICE\&quot; or \&quot;RE\&quot;. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


