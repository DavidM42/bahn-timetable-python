# ReferenceTrip

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | An id that uniquely identifies the reference trip. It consists of the following two elements separated by dashes:  * A &#39;daily trip id&#39; that uniquely identifies a reference trip within one day. This id is typically reused on subsequent days. This could be negative. * A 10-digit date specifier (YYMMddHHmm) that indicates the planned departure date of the referenced regular trip from its start station.  Example:  &#39;-7874571842864554321-1403311221&#39; would be used for a trip with daily trip id &#39;-7874571842864554321&#39; that starts on march the 31th 2014.  | 
**c** | **bool** | The cancellation flag. True means, the reference trip is cancelled. | 
**rtl** | [**ReferenceTripLabel**](ReferenceTripLabel.md) |  | 
**sd** | [**ReferenceTripStopLabel**](ReferenceTripStopLabel.md) |  | 
**ea** | [**ReferenceTripStopLabel**](ReferenceTripStopLabel.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


