# ReferenceTripStopLabel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**i** | **int** | The index of the correspondent stop of the regu-lar trip. | 
**pt** | **str** | The planned time of the correspondent stop of the regular trip. | 
**eva** | **int** | The eva number of the correspondent stop of the regular trip. | 
**n** | **str** | The (long) name of the correspondent stop of the regular trip. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


