# Stop

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ar** | [**Event**](Event.md) | Arrival element. This element does not have child elements. All information about the arrival is stored in attributes (see the next table). | [optional] 
**dp** | [**Event**](Event.md) | Departure element. This element does not have child elements. All information about the departure is stored in attributes (see the next table). | [optional] 
**m** | [**list[Message]**](Message.md) | Message element. | [optional] 
**hd** | [**list[HistoricDelay]**](HistoricDelay.md) | Historic delay element. | [optional] 
**hpc** | [**list[HistoricPlatformChange]**](HistoricPlatformChange.md) | Historic platform change element. | [optional] 
**conn** | [**list[Connection]**](Connection.md) | Connection element. | [optional] 
**rtr** | [**list[ReferenceTripRelation]**](ReferenceTripRelation.md) | Reference trip relation element. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


