# Stations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**station** | [**list[StationData]**](StationData.md) | List of stations with additional data. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


