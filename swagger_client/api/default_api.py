# coding: utf-8

"""
    Timetables

    NUTZUNGSBEDINGUNGEN: Es gelten die gleichen Nutzungsbedingungen wie für die Web Bahnhofstafel: http://www.bahnhof.de/bahnhof-de/nutzungsbedingungen_wbt.html  BETA-TEST: A RESTful web service for timetable information for train stations operated by DB Station&Service AG.   # noqa: E501

    OpenAPI spec version: v1
    Contact: dbopendata@deutschebahn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class DefaultApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def fchg_eva_no_get(self, eva_no, **kwargs):  # noqa: E501
        """Returns all known changes for a station  # noqa: E501

        Returns a Timetable object (see Timetable) that contains all known changes for the station given by evaNo.  The data includes all known changes from now on until ndefinitely into the future. Once changes become obsolete (because their trip departs from the station) they are removed from this resource.  Changes may include messages. On event level, they usually contain one or more of the 'changed' attributes ct, cp, cs or cpth. Changes may also include 'planned' attributes if there is no associated planned data for the change (e.g. an unplanned stop or trip).  Full changes are updated every 30s and should be cached for that period by web caches.   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.fchg_eva_no_get(eva_no, async=True)
        >>> result = thread.get()

        :param async bool
        :param str eva_no: Station EVA-number. (required)
        :return: Timetable
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.fchg_eva_no_get_with_http_info(eva_no, **kwargs)  # noqa: E501
        else:
            (data) = self.fchg_eva_no_get_with_http_info(eva_no, **kwargs)  # noqa: E501
            return data

    def fchg_eva_no_get_with_http_info(self, eva_no, **kwargs):  # noqa: E501
        """Returns all known changes for a station  # noqa: E501

        Returns a Timetable object (see Timetable) that contains all known changes for the station given by evaNo.  The data includes all known changes from now on until ndefinitely into the future. Once changes become obsolete (because their trip departs from the station) they are removed from this resource.  Changes may include messages. On event level, they usually contain one or more of the 'changed' attributes ct, cp, cs or cpth. Changes may also include 'planned' attributes if there is no associated planned data for the change (e.g. an unplanned stop or trip).  Full changes are updated every 30s and should be cached for that period by web caches.   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.fchg_eva_no_get_with_http_info(eva_no, async=True)
        >>> result = thread.get()

        :param async bool
        :param str eva_no: Station EVA-number. (required)
        :return: Timetable
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['eva_no']  # noqa: E501
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method fchg_eva_no_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'eva_no' is set
        if ('eva_no' not in params or
                params['eva_no'] is None):
            raise ValueError("Missing the required parameter `eva_no` when calling `fchg_eva_no_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'eva_no' in params:
            path_params['evaNo'] = params['eva_no']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/xml'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/xhtml+xml', 'application/xml', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Bearer']  # noqa: E501

        return self.api_client.call_api(
            '/fchg/{evaNo}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Timetable',  # noqa: E501
            auth_settings=auth_settings,
            async=params.get('async'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def plan_eva_no_date_hour_get(self, eva_no, date, hour, **kwargs):  # noqa: E501
        """Returns planned data for the specified station within an hourly time slice   # noqa: E501

        Returns a Timetable object (see Timetable) that contains planned data for the specified station (evaNo) within the hourly time slice given by date (format YYMMDD) and hour (format HH). The data includes stops for all trips that arrive or depart within that slice. There is a small overlap between slices since some trips arrive in one slice and depart in another.  Planned data does never contain messages. On event level, planned data contains the 'plannned' attributes pt, pp, ps and ppth while the 'changed' attributes ct, cp, cs and cpth are absent.  Planned data is generated many hours in advance and is static, i.e. it does never change. It should be cached by web caches.public interface allows access to information about a station.   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.plan_eva_no_date_hour_get(eva_no, date, hour, async=True)
        >>> result = thread.get()

        :param async bool
        :param str eva_no: Station EVA-number. (required)
        :param str date: Date in format YYMMDD. (required)
        :param str hour: Hour in format HH. (required)
        :return: Timetable
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.plan_eva_no_date_hour_get_with_http_info(eva_no, date, hour, **kwargs)  # noqa: E501
        else:
            (data) = self.plan_eva_no_date_hour_get_with_http_info(eva_no, date, hour, **kwargs)  # noqa: E501
            return data

    def plan_eva_no_date_hour_get_with_http_info(self, eva_no, date, hour, **kwargs):  # noqa: E501
        """Returns planned data for the specified station within an hourly time slice   # noqa: E501

        Returns a Timetable object (see Timetable) that contains planned data for the specified station (evaNo) within the hourly time slice given by date (format YYMMDD) and hour (format HH). The data includes stops for all trips that arrive or depart within that slice. There is a small overlap between slices since some trips arrive in one slice and depart in another.  Planned data does never contain messages. On event level, planned data contains the 'plannned' attributes pt, pp, ps and ppth while the 'changed' attributes ct, cp, cs and cpth are absent.  Planned data is generated many hours in advance and is static, i.e. it does never change. It should be cached by web caches.public interface allows access to information about a station.   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.plan_eva_no_date_hour_get_with_http_info(eva_no, date, hour, async=True)
        >>> result = thread.get()

        :param async bool
        :param str eva_no: Station EVA-number. (required)
        :param str date: Date in format YYMMDD. (required)
        :param str hour: Hour in format HH. (required)
        :return: Timetable
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['eva_no', 'date', 'hour']  # noqa: E501
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method plan_eva_no_date_hour_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'eva_no' is set
        if ('eva_no' not in params or
                params['eva_no'] is None):
            raise ValueError("Missing the required parameter `eva_no` when calling `plan_eva_no_date_hour_get`")  # noqa: E501
        # verify the required parameter 'date' is set
        if ('date' not in params or
                params['date'] is None):
            raise ValueError("Missing the required parameter `date` when calling `plan_eva_no_date_hour_get`")  # noqa: E501
        # verify the required parameter 'hour' is set
        if ('hour' not in params or
                params['hour'] is None):
            raise ValueError("Missing the required parameter `hour` when calling `plan_eva_no_date_hour_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'eva_no' in params:
            path_params['evaNo'] = params['eva_no']  # noqa: E501
        if 'date' in params:
            path_params['date'] = params['date']  # noqa: E501
        if 'hour' in params:
            path_params['hour'] = params['hour']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/xml'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/xhtml+xml', 'application/xml', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Bearer']  # noqa: E501

        return self.api_client.call_api(
            '/plan/{evaNo}/{date}/{hour}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Timetable',  # noqa: E501
            auth_settings=auth_settings,
            async=params.get('async'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def rchg_eva_no_get(self, eva_no, **kwargs):  # noqa: E501
        """Returns all recent changes for a station   # noqa: E501

        Returns a Timetable object (see Timetable) that contains all recent changes for the station given by evaNo. Recent changes are always a subset of the full changes. They may equal full changes but are typically much smaller. Data includes only those changes that became known within the last 2 minutes.  A client that updates its state in intervals of less than 2 minutes should load full changes initially and then proceed to periodically load only the recent changes in order to save bandwidth.  Recent changes are updated every 30s as well and should be cached for that period by web caches.   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.rchg_eva_no_get(eva_no, async=True)
        >>> result = thread.get()

        :param async bool
        :param str eva_no: Station EVA-number. (required)
        :return: Timetable
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.rchg_eva_no_get_with_http_info(eva_no, **kwargs)  # noqa: E501
        else:
            (data) = self.rchg_eva_no_get_with_http_info(eva_no, **kwargs)  # noqa: E501
            return data

    def rchg_eva_no_get_with_http_info(self, eva_no, **kwargs):  # noqa: E501
        """Returns all recent changes for a station   # noqa: E501

        Returns a Timetable object (see Timetable) that contains all recent changes for the station given by evaNo. Recent changes are always a subset of the full changes. They may equal full changes but are typically much smaller. Data includes only those changes that became known within the last 2 minutes.  A client that updates its state in intervals of less than 2 minutes should load full changes initially and then proceed to periodically load only the recent changes in order to save bandwidth.  Recent changes are updated every 30s as well and should be cached for that period by web caches.   # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.rchg_eva_no_get_with_http_info(eva_no, async=True)
        >>> result = thread.get()

        :param async bool
        :param str eva_no: Station EVA-number. (required)
        :return: Timetable
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['eva_no']  # noqa: E501
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method rchg_eva_no_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'eva_no' is set
        if ('eva_no' not in params or
                params['eva_no'] is None):
            raise ValueError("Missing the required parameter `eva_no` when calling `rchg_eva_no_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'eva_no' in params:
            path_params['evaNo'] = params['eva_no']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/xml'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/xhtml+xml', 'application/xml', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Bearer']  # noqa: E501

        return self.api_client.call_api(
            '/rchg/{evaNo}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Timetable',  # noqa: E501
            auth_settings=auth_settings,
            async=params.get('async'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def station_pattern_get(self, pattern, **kwargs):  # noqa: E501
        """Returns information about stations matching the given pattern  # noqa: E501

        This public interface allows access to information about a station.  See the document \"Types and XML Schema Definition\" in the documentation tab for information about the resulting data structure.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.station_pattern_get(pattern, async=True)
        >>> result = thread.get()

        :param async bool
        :param str pattern: can be a station name (prefix), eva number, ds100/rl100 code, wildcard (*); doesn't seem to work with Umlauten in station name (prefix) (required)
        :return: Stations
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async'):
            return self.station_pattern_get_with_http_info(pattern, **kwargs)  # noqa: E501
        else:
            (data) = self.station_pattern_get_with_http_info(pattern, **kwargs)  # noqa: E501
            return data

    def station_pattern_get_with_http_info(self, pattern, **kwargs):  # noqa: E501
        """Returns information about stations matching the given pattern  # noqa: E501

        This public interface allows access to information about a station.  See the document \"Types and XML Schema Definition\" in the documentation tab for information about the resulting data structure.  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async=True
        >>> thread = api.station_pattern_get_with_http_info(pattern, async=True)
        >>> result = thread.get()

        :param async bool
        :param str pattern: can be a station name (prefix), eva number, ds100/rl100 code, wildcard (*); doesn't seem to work with Umlauten in station name (prefix) (required)
        :return: Stations
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['pattern']  # noqa: E501
        all_params.append('async')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method station_pattern_get" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'pattern' is set
        if ('pattern' not in params or
                params['pattern'] is None):
            raise ValueError("Missing the required parameter `pattern` when calling `station_pattern_get`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'pattern' in params:
            path_params['pattern'] = params['pattern']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/xml'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/xhtml+xml', 'application/xml', 'text/html'])  # noqa: E501

        # Authentication setting
        auth_settings = ['Bearer']  # noqa: E501

        return self.api_client.call_api(
            '/station/{pattern}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='Stations',  # noqa: E501
            auth_settings=auth_settings,
            async=params.get('async'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
