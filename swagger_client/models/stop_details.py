# coding: utf-8

"""
    Timetables

    NUTZUNGSBEDINGUNGEN: Es gelten die gleichen Nutzungsbedingungen wie für die Web Bahnhofstafel: http://www.bahnhof.de/bahnhof-de/nutzungsbedingungen_wbt.html  BETA-TEST: A RESTful web service for timetable information for train stations operated by DB Station&Service AG.   # noqa: E501

    OpenAPI spec version: v1
    Contact: dbopendata@deutschebahn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from swagger_client.models.connection import Connection  # noqa: F401,E501
from swagger_client.models.message import Message  # noqa: F401,E501


class StopDetails(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'm': 'list[Message]',
        'conn': 'list[Connection]'
    }

    attribute_map = {
        'id': 'id',
        'm': 'm',
        'conn': 'conn'
    }

    def __init__(self, id=None, m=None, conn=None):  # noqa: E501
        """StopDetails - a model defined in Swagger"""  # noqa: E501

        self._id = None
        self._m = None
        self._conn = None
        self.discriminator = None

        self.id = id
        if m is not None:
            self.m = m
        if conn is not None:
            self.conn = conn

    @property
    def id(self):
        """Gets the id of this StopDetails.  # noqa: E501

        Stop id.  # noqa: E501

        :return: The id of this StopDetails.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this StopDetails.

        Stop id.  # noqa: E501

        :param id: The id of this StopDetails.  # noqa: E501
        :type: str
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def m(self):
        """Gets the m of this StopDetails.  # noqa: E501

        Message.  # noqa: E501

        :return: The m of this StopDetails.  # noqa: E501
        :rtype: list[Message]
        """
        return self._m

    @m.setter
    def m(self, m):
        """Sets the m of this StopDetails.

        Message.  # noqa: E501

        :param m: The m of this StopDetails.  # noqa: E501
        :type: list[Message]
        """

        self._m = m

    @property
    def conn(self):
        """Gets the conn of this StopDetails.  # noqa: E501

        Message.  # noqa: E501

        :return: The conn of this StopDetails.  # noqa: E501
        :rtype: list[Connection]
        """
        return self._conn

    @conn.setter
    def conn(self, conn):
        """Sets the conn of this StopDetails.

        Message.  # noqa: E501

        :param conn: The conn of this StopDetails.  # noqa: E501
        :type: list[Connection]
        """

        self._conn = conn

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, StopDetails):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
