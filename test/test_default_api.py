# coding: utf-8

"""
    Timetables

    NUTZUNGSBEDINGUNGEN: Es gelten die gleichen Nutzungsbedingungen wie für die Web Bahnhofstafel: http://www.bahnhof.de/bahnhof-de/nutzungsbedingungen_wbt.html  BETA-TEST: A RESTful web service for timetable information for train stations operated by DB Station&Service AG.   # noqa: E501

    OpenAPI spec version: v1
    Contact: dbopendata@deutschebahn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.default_api import DefaultApi  # noqa: E501
from swagger_client.rest import ApiException


class TestDefaultApi(unittest.TestCase):
    """DefaultApi unit test stubs"""

    def setUp(self):
        self.api = swagger_client.api.default_api.DefaultApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_fchg_eva_no_get(self):
        """Test case for fchg_eva_no_get

        Returns all known changes for a station  # noqa: E501
        """
        pass

    def test_plan_eva_no_date_hour_get(self):
        """Test case for plan_eva_no_date_hour_get

        Returns planned data for the specified station within an hourly time slice   # noqa: E501
        """
        pass

    def test_rchg_eva_no_get(self):
        """Test case for rchg_eva_no_get

        Returns all recent changes for a station   # noqa: E501
        """
        pass

    def test_station_pattern_get(self):
        """Test case for station_pattern_get

        Returns information about stations matching the given pattern  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
