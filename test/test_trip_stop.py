# coding: utf-8

"""
    Timetables

    NUTZUNGSBEDINGUNGEN: Es gelten die gleichen Nutzungsbedingungen wie für die Web Bahnhofstafel: http://www.bahnhof.de/bahnhof-de/nutzungsbedingungen_wbt.html  BETA-TEST: A RESTful web service for timetable information for train stations operated by DB Station&Service AG.   # noqa: E501

    OpenAPI spec version: v1
    Contact: dbopendata@deutschebahn.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.models.trip_stop import TripStop  # noqa: E501
from swagger_client.rest import ApiException


class TestTripStop(unittest.TestCase):
    """TripStop unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testTripStop(self):
        """Test TripStop"""
        # FIXME: construct object with mandatory attributes with example values
        # model = swagger_client.models.trip_stop.TripStop()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
